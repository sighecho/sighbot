use std::env;

use serenity::{
    model::{channel::Message, gateway::Ready}
    prelude::*
}

struct Handler;

impl EventHandler for Handler
{
    fn message(&self, ctx: Context, msg: Message)
    {
        if msg.Content == "!ping"
        {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong")
            {
                println!("Error sending message: {:?}", why)
            }
        }
    }

    fn ready(&self, _: Context, ready: Ready)
    {
        println!("{} is connected", ready.user.name)
    }
}

fn main()
{
    let token = env::var("DISCORD_TOKEN").expect("Expected a token environment variables");

    let mut client = Client::new(&token, Handler).expect("Error creating client");

    if let Err(errorMessage) = client.start()
    {
        println!("Client error: {:?}", errorMessage);
    }
}